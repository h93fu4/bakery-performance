import { TimeService } from './time.service';
import { Component } from '@angular/core';
import { ClockComponent } from './clock/clock.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bakery';
  updateTime: number;
  status: string;
  strategy: string;

  constructor(private timeService: TimeService) {}

  ngOnInit() {
    this.updateTime = this.timeService.countTime();

    // 每秒更新一次時間
    setInterval(() => {
      this.updateTime = this.timeService.countTime();
    }, 1000);
  }

  // 判斷狀態
  getStatus(time) {
    if(time = 8) {
      this.status = '準備中'
    } else if(time >= 9 && time < 16) {
      this.status = '營業中'
    } else if(time = 16) {
      this.status = '關店收帳中'
    } else {
      this.status = '休息中'
    }
  }

  //手動提早關店
  setClose() {
    if(this.updateTime >= 12) {
      this.status = '關店收帳中'
    }
  }

  //手動提早休息
  setRest() {
    if(this.updateTime >= 13) {
      this.status = '休息中'
    }
  }

}
