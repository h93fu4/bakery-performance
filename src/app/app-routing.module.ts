import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreReportComponent } from './store-report/store-report.component';


const routes: Routes = [
  { path: 'store-report/:id', component: StoreReportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
