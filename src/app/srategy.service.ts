import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SrategyService {

  constructor() { }

  // // 策略介面
  // public interface IStrategy {
  //   public int caculate(int a , int b);
  // }

  // // 實現策略介面
  // public class QuantityUpgrade implements IStrategy {
  //   public int caculate(int a, int b) {
  //       return a + b;
  //   }
  // }

  // public class PremiumIngredients implements IStrategy {
  //     public int caculate(int a, int b) {
  //         return a - b ;
  //     }
  // }

  // public class DecorUpgrade implements IStrategy {
  //     public int caculate(int a, int b) {
  //         return a * b;
  //     }
  // }

}
