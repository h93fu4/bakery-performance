import { Component, OnInit } from '@angular/core';
import { WorkerFactoryService } from './../worker-factory.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@Component({
  selector: 'app-store-report',
  templateUrl: './store-report.component.html',
  styleUrls: ['./store-report.component.css']
})
export class StoreReportComponent implements OnInit {

  //報表一(員工產值)data
  workerData: { name: string, value: number }[];
  jsonData: any ;
  view: any[] = [700, 400];
  

  // options
  showXAxis: boolean = false;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  showXAxisLabel: boolean = false;
  yAxisLabel: string = '';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = '';
  
  public customValueFormatting = (value: any) => `${value}件`;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private workerFactoryService: WorkerFactoryService) { }

  ngOnInit() {
    // this.getWorker('Manager', '份量提升', '準備中');
    // console.log( this.jsonData)
    this.getWorker('Manager', '份量提升', '準備中')
  }

  //產生報表一(員工產值)
  getWorker(type: string, strategy: string, status: string): void {
    this.jsonData = this.workerFactoryService.getData('Manager', '份量提升', '準備中')
  }

}
