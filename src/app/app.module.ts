import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreReportComponent } from './store-report/store-report.component';
import { EmployeeChartComponent } from './employee-chart/employee-chart.component';
import { SalesChartComponent } from './sales-chart/sales-chart.component';
import { ClockComponent } from './clock/clock.component';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StoresComponent } from './stores/stores.component';

@NgModule({
  declarations: [
    AppComponent,
    StoreReportComponent,
    EmployeeChartComponent,
    SalesChartComponent,
    ClockComponent,
    StoresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
