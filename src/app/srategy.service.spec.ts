import { TestBed } from '@angular/core/testing';

import { SrategyService } from './srategy.service';

describe('SrategyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SrategyService = TestBed.get(SrategyService);
    expect(service).toBeTruthy();
  });
});
