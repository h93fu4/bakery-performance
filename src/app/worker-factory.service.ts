import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class WorkerFactoryService {
  constructor(
  ) {}

  createWorker(type: string, strategy: string, status: string) {
    switch (type) {
      case 'Manager':
        return new Manager(strategy, status); // 創建新的 WorkerService 實例並傳遞 strategy 和 status 參數
      case 'Cashier':
        return new Cashier(strategy, status);
      case 'Baker':
        return new Baker(strategy, status);
      default:
        throw new Error('Invalid worker type');
    }
  }

   getData(type: string, strategy: string, status: string): { name: string, value: number }[] {
    const data: { name: string, value: number }[] = [];

    // 產生店經理data
    const manager = this.createWorker('Manager', '份量提升', '準備中');
    data.push({ name: '店經理', value: manager.getAbility() });

    // 產生收營員data
    const cashier1 = this.createWorker('Cashier', '份量提升', '準備中');
    const cashier2 = this.createWorker('Cashier', '份量提升', '準備中');
    data.push({ name: '收營員 1', value: cashier1.getAbility() });
    data.push({ name: '收營員 2', value: cashier2.getAbility() });

    // 產生麵包師data
    const baker1 = this.createWorker('Baker', '份量提升', '準備中');
    const baker2 = this.createWorker('Baker', '份量提升', '準備中');
    data.push({ name: '麵包師 1', value: baker1.getAbility() });
    data.push({ name: '麵包師 2', value: baker2.getAbility() });

    return data;
  }
}

interface Worker {
  getAbility();
}

// 具體員工種類 經理
class Manager implements Worker {
  constructor(private strategy: string, private status: string) {
    // 生成不同範圍內的隨機數作為baseValue
    this.baseValue = this.calculateBaseValue();
  }

  private baseValue: number;

  private calculateBaseValue(): number {
    let min = 0;
    let max = 0;

    if (this.status === '準備中') {
      min = 70;
      max = 90;
    } else if (this.status === '營業中') {
      min = 60;
      max = 90;
    } else if (this.status === '關店收帳中') {
      return 90;
    } else {
      return 0;
    }

    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getAbility(): number {
    let ability = this.baseValue;

    if (this.strategy === '高級原料' || this.strategy === '提升裝潢') {
      ability += 10;
    } 
    return ability;
  }
}

// 具體員工種類 收營員
class Cashier implements Worker {
  constructor(private strategy: string, private status: string) {

    this.baseValue = this.calculateBaseValue();
  }

  private baseValue: number;

  private calculateBaseValue(): number {
    let min = 0;
    let max = 0;

    if (this.status === '準備中' || this.status === '休息中') {
      return 0;
    } else if (this.status === '營業中') {
      min = 60;
      max = 90;
    } else if (this.status === '關店收帳中') {
      return 60;
    }

    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getAbility(): number {
    let ability = this.baseValue;

    if (this.strategy === '高級原料') {
      ability += 20;
    } else if (this.strategy === '提升裝潢') {
      ability += 10;
    }
    return ability;
  }
}

// 具體員工種類 麵包師
class Baker implements Worker {
  constructor(private strategy: string, private status: string) {

    this.baseValue = this.calculateBaseValue();
  }

  private baseValue: number;

  private calculateBaseValue(): number {
    let min = 0;
    let max = 0;

    if (this.status === '準備中') {
      min = 70;
      max = 100;
    } else if (this.status === '營業中') {
      min = 20;
      max = 90;
    } else {
      return 0;
    }

    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getAbility(): number {
    let ability = this.baseValue;

    if (this.strategy === '份量提升') {
      ability += 20;
    }
    return ability;
  }
}