import { TestBed } from '@angular/core/testing';

import { WorkerFactoryService } from './worker-factory.service';

describe('WorkerFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkerFactoryService = TestBed.get(WorkerFactoryService);
    expect(service).toBeTruthy();
  });
});
