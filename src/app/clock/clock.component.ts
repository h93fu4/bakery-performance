import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {
  private startTime: number; // 當前時間的毫秒數
  private clock: string; // 顯示的時間
  private hour: number; // 用來判斷營業狀態

  constructor() {
    this.startTime = new Date().getTime(); // 初始化 startTime
  }

  ngOnInit() {
    this.countTime();
  }

  countTime() {
    var currentTime = new Date().getTime();
    var passTime = currentTime - this.startTime; // 過去的毫秒數

    // 轉換為模擬時間
    var simulatedTime = (passTime * 120) / 1000; // 轉換為秒數

    // 轉換為小時、分鐘
    var hh = Math.floor(simulatedTime / 3600 % 24);
    var remainingSeconds = simulatedTime % 3600; // 取得餘下的秒數
    var mm = Math.floor(remainingSeconds / 60);

    hh = this.checkTime(hh);
    mm = this.checkTime(mm);

    this.hour = hh;
    this.clock = hh + ":" + mm ;
    var timeoutId = setTimeout(() => {
      this.countTime();
      }, 1000);
    }

  checkTime(i) {
    if(i < 10) {
      i = "0" + i;
    }
    return i;
  }

}
